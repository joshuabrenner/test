function stim_type = StimTyper(stim_struct)
% type = StimTyper(stim_struct)
% help goes here
% 

stim_type = [];

if isfield (stim_struct, 'type'),
    stim_type = stim_struct.type;
    return
end

if isfield (stim_struct,'switch_frame'),

    stim_type = 'hybrid';

elseif isfield (stim_struct,'sect_deg'),

    stim_type = 'scramble';

elseif isfield (stim_struct,'smooth'),

    stim_type = 'smooth';

elseif isfield (stim_struct,'collapse'),

    stim_type = 'fixed';

elseif isfield(stim_struct, 'sqr_deg'),

    stim_type = 'random';

elseif isfield(stim_struct,'delay')

    stim_type = 'summate';

elseif isfield(stim_struct, 'direction'),

    stim_type = 'bar';

elseif isfield(stim_struct, 'arc_angle'),

    stim_type = 'pie';

% elseif isfield(stim_struct, 'approach'),
%         
%     stim_type = 'angled';

elseif isfield(stim_struct, 'object'),
        
    stim_type = 'loom';

end


