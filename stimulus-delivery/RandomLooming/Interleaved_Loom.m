function Interleaved_Loom(tag,stim_struct,loom_struct, Xcent, Ycent, varargin)
%



acctime = 1; %This is the time the animal can explore the arena without triggering
%any presentation of stimuli, in seconds
loomtail = 2; %This is the time you want the stimulus to remain on the screen after
%presentation, in seconds
cooldown = 5; %This is the cooldown time between stimuli - the mouse will still
%have to trigger the next presentation, but triggering will be blocked for
%this period
repeats = 9; %This is the number of times the stimulus will be presented before the 
%trial ends. MUST BE DIVISIBLE BY 3 IN THIS TRIAL STRUCTURE

%Combine the grating and the bars (dot) and fullfield
trials = {'standard';'chunky';'spatial'};
    
% Josh ala Ric % Ordered repetitions and Randomize each repetition
    % RANDOMIZE and INTERLEAVE
        
        
     %Randomize the set of trials and repeat the randomization at each repetition  
        RandTrials = trials;
        loom_list = trials;
        for i= 1:repeats
            
            %  % If the user has selected randomize we randomize the trials by using a
               % random permutation of the trials. The same set of trials
               % is re-randomized at every repetition
           RandTrials = trials(randperm(numel(trials)));
           loom_list = [loom_list ; RandTrials];
        end
save(tag, 'loom_list');



% 
% loom_list = cell(1,repeats); %If you want more or fewer trials, just change the numer of cells
% randnums = randperm(repeats);
% for i = 1:repeats
%     if randnums(i) <= repeats / 3
%         loom_list{i} = 'standard';
%     elseif randnums(i) <= (repeats * 2 / 3)
%         loom_list{i} =  'chunky';
%     else
%         loom_list{i} = 'spatial';
%     end
% end


% stim_presenter(stim_struct,varargin)
% stim_presenter uses PTB to present visual stimuli as produced by stim_object or
% stim_scrambler. If stim_struct contains standard looming stimulus, no
% further arguments are needed. Otherwise at least one additional parameter
% must be given.
%
% accepts parameters in varargin of:
%   'temporal'      - temporally scrambled looming stimulus
%   'chunky'        - chunked into receptive field size squares
%   'constant rate' - looming stimulus with constant rate luminance change
%   'same time'     - constant rate looming stimulus with fixed timing
% additionally can use parameter 'spatial' with any of the above
%   'spatial'       - spatially scrambled stimulus
%

load loom_stim_defaults.mat
load display_settings.mat clut hide_cursor frame_rate
[~,lum_max] = max( clut );
clut = 1:255;

new_win = true;
new_ttl = true;
grey = [];
pulse = true;
mask = [];
rep_del = 0;
gray = [];
mask_id = 0;
mask_color = round(lum_max*0.5);
switch_frame = 0;

if nargin == 0,
	help stim_presenter
	return
end
errstr = sprintf(['\n Must specify which nontraditional loom to be used.\n' ...
	'Choices are: ''temporal'', ''constant rate'', ''chunky''']);

string = cell(length(varargin),1);
set_args = nargin - length(varargin);
for n = 1:length(varargin),
	string{n} = inputname(n+set_args);
	if strcmpi(string{n}, 'repeats') && isnumeric(varargin{n}),
		repeats = round(varargin{n});
		if isnumeric(varargin{n+1}),
			rep_del = round(varargin{n+1});
		else
			rep_del = 2;
		end
	end
end
% 
% % clut = select_lut( location );	% add to present setup after you write it
% white = 255;% max(clut);				% white=WhiteIndex(window);
% black = 0;%min(clut);				% black=BlackIndex(window);
% 
% code_list = {};
% 
% 
% 
% background = [ 128 128 128 ];
% foreground = [ 1 1 1];


%%% This section works the gamma correction!
monitorInformation;
    screenNumber = monitorInfo.screenNumber;

    % COLOR INFORMATION OF SCREEN
    % Get black, white and gray color values for the current monitor
    white = WhiteIndex(screenNumber);
    black = BlackIndex(screenNumber);
    
    %Convert balck and white to luminance values to determine gray
    %luminance
    whiteLum = PixToLum(white);
    blackLum = PixToLum(black);
    grayLum = (whiteLum + blackLum)/2;
    
    % Now determine the pixel value of gray from the gray luminance
    gray = GammaCorrect(grayLum);
    clut = GammaCorrect(1:.52549:134);

% clut = select_lut( location );	% add to present setup after you write it
% white = 255;% max(clut);				% white=WhiteIndex(window);
% black = 0;%min(clut);				% black=BlackIndex(window);

code_list = {};

stim_type = StimTyper(stim_struct)';
varargin = {'spatial'};
present_setup_rig;

background = gray;
foreground = black;

%%% End gamma correction

if new_win  %This section places the window in the center of the looming stim monitor setup but will need to be changed for other setups		
	screens = Screen( 'Screens' );
	screenNumber = 0;
	[window, windowRect] = Screen('OpenWindow', screenNumber, background,[-1921 0 -1 2160]); %JB - the final argument here lets you pick the 
    %center of your window

end

WaitSecs(2);
flipRate = Screen('GetFlipInterval', window);
if flipRate > 1.025/frame_rate,
	disp( [num2str(flipRate), ' refresh rate to low!'] )
	disp( 'trying again' )
	WaitSecs(2);
	flipRate = Screen('GetFlipInterval', window);
	if flipRate > 1.025/frame_rate,
		disp( [num2str(flipRate), ' refresh rate to low!'] )
		disp('canceling stimulus')
		Screen('CloseAll')
		return
	end
end


%% Interframe info is important for vbl (timing) calculations
% INTERFRAME INTERVAL INFO   
    % Get the montior inter-frame-interval 
    ifi = Screen('GetFlipInterval',window);
    
    %on old slow machines we may not be able to update every ifi. If your
    %graphics processor is too slow you can buy a better one or adjust the
    %number of frames to wait between flips below
    
    waitframes = 1; %I expect most new computers can handle updates at ifi
    ifiDuration = waitframes*ifi;

    %% %%%%%%%%%%%%%%%%%%%%% PAUSE FOR ACCLIMATION %%%%%%%%%%%%%%%%%%%%%%%%%%%
        %At the start of the trial, we want to wait for some period of time defined by acctime to allow
        %the animal to acclimate to the arena
        % Flip the screen and collect the time of the flip
        vbl=Screen('Flip', window);
        
        % We will loop until delay time referenced to the flip time
        acctime = vbl + acctime;
        % 
        while (vbl < acctime)
            % Draw the fully loomed stim
            Screen('FillRect', window,background);
            
            % update the vbl timestamp and provide headroom for jitters
            vbl = Screen('Flip', window, vbl + (waitframes - 0.5) * ifi);
            
            % exit the while loop and flag to one if user presses any key
            if KbCheck
                exitLoop=1;
                break;
            end
        end
    

%%
%-------------------------------------------------------------------------%
%----------------------- Prepare presentation ------------------------%
%-------------------------------------------------------------------------%

if hide_cursor
	HideCursor
	priorityLevel = MaxPriority(window);
	Priority(priorityLevel);
end
Screen( window, 'FillRect', background);
Screen( window, 'Flip');


setup_string = 'spatial';%This lets you adjust for the center of the loom without
%generating any stimulus - spatial could be changed to chunky with no
%difference
present_setup_rig_interleaved_random;
if ~strcmp(stim_type,'loom') %JB 6/11 - this prevents standard looms from causing an error (there lum_mat will not be multiplied
    %by 2 otherwise due to some changes I made in the present setup rig
    %version above)
    lum_mat = lum_mat*2;
end
    for i = 1:length(squares) %JB 6/12 this should allow you to alter the center
        %of the loom according the X and Y center inputs for chunky and
        %scrambled stimuli
        squares(1,i) = squares(1,i) + Xcent;
        squares(3,i) = squares(3,i) + Xcent;
        squares(2,i) = squares(2,i) + Ycent;
        squares(4,i) = squares(4,i) + Ycent;
    end
    for i = 1:length(loom)%JB 6/12 this should allow you to alter the center
        %of the loom according the X and Y center inputs for standard looms
        loom{1,i}(1,1) = loom{1,i}(1,1) + Xcent;
        loom{1,i}(1,2) = loom{1,i}(1,2) + Ycent;
        loom{1,i}(1,3) = loom{1,i}(1,3) + Xcent;
        loom{1,i}(1,4) = loom{1,i}(1,4) + Ycent;
        
        loom{2,i}(1,1) = loom{2,i}(1,1) + Xcent;
        loom{2,i}(1,2) = loom{2,i}(1,2) + Ycent;
        loom{2,i}(1,3) = loom{2,i}(1,3) + Xcent;
        loom{2,i}(1,4) = loom{2,i}(1,4) + Ycent;
    end


if any(strcmp(string, 'frame')) && isnumeric(varargin{strcmp(string, 'frame')})
	frame_array = varargin{ strcmp( string, 'frame') };
else
	frame_array = 1:frames;
end
VBLT = zeros(1,frames);

clear MegaArduino
MegaArduino = arduino();
for rp = 1:repeats,
    setup_string = loom_list(rp);%This lets you load setup info for both the chunky and the scrambled (aka spatial) stimuli.
    %You'll notice I already did this above. It's necessary to do it like
    %this because we are trying to accomplish two different things - in the
    %first case we just want to move the center of the stimulus, in this
    %case we are calling up a different stimulus data *for every
    %repetition*. So, if we were to perform the center shift component
    %inside of this loop, we would end up shifting the center every single
    %time we ran through, which is not good.
    present_setup_rig_interleaved_random;
if ~strcmp(stim_type,'loom') %JB 6/11 - this prevents standard looms from causing an error
    lum_mat = lum_mat*2;
end
    %% 
%-------------------------------------------------------------------------%
%--------------------- Wait for Presentation Trigger ---------------------%
%-------------------------------------------------------------------------%

loom_list(rp) %this gives a readout of what kind of stimulus you are playing. Below
%is the trigger code for the arduino which you might or might not want
%depending on your setup.
% 
% while 1 %Create an infinite loop
% 
% % exit the while loop and flag to one if user presses any key
%         if KbCheck
%             exitLoop=1;
%             break;
%         end
% 
% %Check the voltage of the arduino - if less than 2.5, keep looping. If
% %greater, cut to stimulus
%     if MegaArduino.readVoltage(2) < 2.5     % % If arduino sends low voltage (animal isn't present) hold the grey screen - otherwise we will present the stimulus
%     %Display a gray screen            
%     vbl=Screen('Flip', window);
%     % The first time element of the stimulus is the delay from trigger
%     % onset to stimulus onset
%     TimeGray = vbl + 0.1; % 0.1 second
%     % Display a blank screen while the vbl is less than delay time. NOTE
%     % we are going to add 0.5*ifi to the vbl to give us some headroom
%     % to take possible timing jitter or roundoff-errors into account.
%         while (vbl < TimeGray)
%         % Draw a screen with shade matched to screen shade user chose
%         Screen('FillRect', window,background);
%         % update the vbl timestamp and provide headroom for jitters
%         vbl = Screen('Flip', window, vbl + (waitframes - 0.5) * ifi);
%         % exit the while loop and flag to one if user presses any key
%             if KbCheck
%             exitLoop=1;
%             break;
%             end
%         end
%     
%       
% 
%     else
%         break;
%    
%     end
% end
% 
        n = 0;
%%
%%%%%%%%%%%%%%%%%%%GENERATE STIMULI%%%%%%%%%%%%%%%%%%%%%%
    
	for t = frame_array,
		if strcmp( 'standard', loom_list(rp)), %This draws the standard loom
            	n = n+1;
            	Screen('FillRect', window, background, squares);
                % Draw a box at the bottom right of the screen to record
                % all screen flips using a photodiode. Please see the file
                % FlipCheck.m in the stimulus directory for further
                % explanation
                FlipCheck(window, windowRect, [white, black], n)
			if aliasing,
				Screen('FrameRect', window, clut(gray(t)+1), loom{2,t}');
			end
			Screen('FillRect', window, foreground, loom{1,t}');
            
		elseif ~strcmp( 'standard', loom_list(rp)), %This draws a chunky or spatially incoherent looom
			lum_val = clut(lum_mat(:,t) + 2);
            grey = [lum_val; lum_val; lum_val];
			Screen('FillRect', window, grey, squares);
       
		        n = n+1;
            	Screen('FillRect', window, grey, squares);
                % Draw a box at the bottom right of the screen to record
                % all screen flips using a photodiode. Please see the file
                % FlipCheck.m in the stimulus directory for further
                % explanation
                FlipCheck(window, windowRect, [white, black], n)
        end
		VBLT(t) = Screen('Flip',window);
		
		if t == switch_frame,
			stim_type = stim_struct.hybrid{2};
		end
	end
	
	WaitSecs(loomtail);
    
    %% %%%%%%%%%%%%%%%%%%%%% DRAW INTERSTIMULUS SCREEN %%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Between trials we want to draw a blank screen for a time of wait
        
        % Flip the screen and collect the time of the flip
        vbl=Screen('Flip', window);
        
        % We will loop until delay time referenced to the flip time
        waitTime = vbl + cooldown;
        % 
        while (vbl < waitTime)
            % Draw a gray screen
            Screen('FillRect', window, background);
            
            % update the vbl timestamp and provide headroom for jitters
            vbl = Screen('Flip', window, vbl + (waitframes - 0.5) * ifi);
            
            % exit the while loop and flag to one if user presses any key
            if KbCheck
                exitLoop=1;
                break;
            end
        end        
      

end
%% 

if hide_cursor
	Priority(0);
	ShowCursor;
end

s2Diffs = diff(VBLT);

% Check for skipped frames:
framerate = 1/median(s2Diffs);
fps = 1/mean(s2Diffs);
time = max(VBLT)-min(VBLT);
frames2 = sum(s2Diffs)*framerate-frames;

% Close the on- and off-screen windows
if new_win
	Screen('CloseAll')
end

if round(frames2) <= 0
	fprintf('\n The movie was frame-accurate.')
else
	fprintf('\n The movie ran over by %.0f frames.', frames2)
end
fprintf('\n l/|v| was %3.0f ms, mean frame rate %5.2f Hz, \n', lv, fps)
fprintf(' & distance was %3.0f mm. Stim took %5.2f s. \n\n', x_as, time)
disp(code_list)

file = which('loom_stim_defaults.mat');
save (file, 'old_distance', '-append')


